import math
arra=input('请输入一个以逗号为间隔的整型数组:')
A=list(map(int,arra.strip().split(',')))
length=len(A)
print(A)

def MergeSort(A,p,r):
    if r>p:
        q=math.floor((p+r)/2)
        MergeSort(A,p,q)
        MergeSort(A,q+1,r)
        Merge(A,p,q,r)

#数组A可截成排好序的两段，再合并成排好序的一个数组
def Merge(A,p,q,r):
    n1=q-p+1
    n2=r-q
    L=[0]*(n1+1)
    R=[0]*(n2+1)
    #将A分为两个新数组
    for i in range(n1):
        L[i]=A[p+i]
    for j in range(n2):
        R[j]=A[q+1+j]
    L[n1]=float('inf')
    R[n2]=float('inf')
    #合并
    i=0
    j=0
    for k in range(p,r+1):
        if L[i]<=R[j]:
            A[k]=L[i]
            i=i+1
        else:
            A[k]=R[j]
            j=j+1


MergeSort(A,0,length-1)
print(A)
